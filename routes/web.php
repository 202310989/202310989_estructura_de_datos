<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RecursividadController;
use App\Http\Controllers\ApuntadoresController;
use App\Http\Controllers\MemoriaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/Recursividad', [RecursividadController::class, 'incrementable']);
Route::get('/apuntadores', [ApuntadoresController::class, 'Apuntadores']);
Route::get('/memoria', [MemoriaController::class, 'Memoria']);
