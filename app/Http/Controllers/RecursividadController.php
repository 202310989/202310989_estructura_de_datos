<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RecursividadController extends Controller
{
    public function recursividad($Nfin, $N){
        $N += 1;
        if ($N<=$Nfin){
            echo $N;
            $this->recursividad($Nfin, $N);
        }
    }
    public function incrementable(){
        $this->recursividad(25, 1);
        return view('Recursividad', ['Nfin'=>$Nfin]);

        }
}
