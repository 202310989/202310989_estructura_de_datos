<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemoriaController extends Controller
{
    public function Memoria(){
        $resultado = "";
        $resultado.="Memoria en uso". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
        $resultado.=str_repeat("a", 2000000)."<br>";
        $resultado.="Memoria en uso". memory_get_usage() . ' ('. ((memory_get_usage() / 1024) / 1024) .'M) <br>';
        $resultado.='Memoria limite: ' . ini_get('memory_limit') . '<br>';  
        return view('memoria',['resultado'=>$resultado]);
    }
}
